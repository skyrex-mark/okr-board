import React, { Component } from "react";
import "./App.css";
import Board from "./component/Board";
class App extends Component {
  render() {
    return (
      <>
        <Board />
      </>
    );
  }
}

export default App;
