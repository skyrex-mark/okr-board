import React from "react";
import { Button, Card, Input } from "semantic-ui-react";
import "./styles.scss";
class ForecastCard extends React.Component {
  state = {
    forecastList: [
      "Passive reorder notifications",
      "New self-serve flow for distributors"
    ],
    input: ""
  };
  onChangeHandler = event => {
    this.setState({
      input: event.target.value
    });
  };
  onAddItemHandler = () => {
    this.setState(prevState => ({
      forecastList: [...prevState.forecastList, this.state.input],
      input: ""
    }));
  };
  onDeleteHandler = () => index => {
    // let forecastList = [...this.state.forecastList];
    // forecastList.splice(index, 1);
    // this.setState({
    //   forecastList: forecastList
    // });
  };
  onKeyPressHandler = event => {
    if (event.key === "Enter") {
      this.setState(prevState => ({
        forecastList: [...prevState.forecastList, this.state.input],
        input: ""
      }));
    }
  };

  componentDidMount() {
    //TODO: Add firebase conneciton
  }
  render() {
    const { forecastList, input } = this.state;
    return (
      <Card>
        <Card.Content header="Next 4 Weeks - Projects" />
        <Card.Content>
          <Card.Group>
            {forecastList.map((description, index) => (
              <Card fluid key={description}>
                <Card.Content>{description}</Card.Content>
                <Button icon="delete" onClick={this.onDeleteHandler(index)} />
              </Card>
            ))}
          </Card.Group>
        </Card.Content>
        <Card.Content>
          <Input
            fluid
            icon={<Button icon="add" onClick={this.onAddItemHandler} />}
            value={input}
            onChange={this.onChangeHandler}
            onKeyPress={this.onKeyPressHandler}
          />
        </Card.Content>
      </Card>
    );
  }
}
export default ForecastCard;
