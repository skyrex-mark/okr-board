import { withMarkdownNotes } from "@storybook/addon-notes";
import { storiesOf } from "@storybook/react";
import React from "react";
import ForecastCard from "./index";
import ForecastCardDoc from "./index.md";

storiesOf("Component", module).add(
  "ForecastCard",
  withMarkdownNotes(ForecastCardDoc)(() => <ForecastCard />)
);
