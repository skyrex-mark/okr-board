import React from "react";
import { Button, Card, Dropdown, Input, Label } from "semantic-ui-react";
import "./styles.scss";

const priorityOptions = [
  { key: "p1", value: "p1", text: "P1" },
  { key: "p2", value: "p2", text: "P2" },
  { key: "p3", value: "p3", text: "P3" }
];
class PriorityCard extends React.Component {
  state = {
    priorityList: [
      { priority: "p1", task: "Close deal with TLM Foods" },
      { priority: "p2", task: "New Order flow Spec'd" }
    ],
    input: "",
    priority: ""
  };
  onChangeHandler = event => {
    this.setState({
      input: event.target.value
    });
  };
  onAddItemHandler = () => {
    console.log(this.state.input);
    console.log(this.state.priority);
    this.setState(prevState => ({
      priorityList: [
        ...prevState.priorityList,
        { task: this.state.input, priority: this.state.priority }
      ],
      input: ""
    }));
  };
  onDeleteHandler = event => index => {
    // let forecastList = [...this.state.forecastList];
    // forecastList.splice(index, 1);
    // this.setState({
    //   forecastList: forecastList
    // });
  };
  onKeyPressHandler = event => {
    if (event.key === "Enter") {
      this.setState(prevState => ({
        forecastList: [...prevState.forecastList, this.state.input],
        input: ""
      }));
    }
  };

  onChangeSelectionHandler = (event, { value }) => {
    console.log(value);
    this.setState({
      priority: value
    });
  };

  getLabelColor = label => {
    const color = { p1: "green", p2: "yellow", p3: "blue" };
    return color[label];
  };
  componentDidMount() {
    //TODO: Add firebase conneciton
  }
  render() {
    const { priorityList, input, priority } = this.state;
    return (
      <Card>
        <Card.Content header="Priorities this week" />
        <Card.Content>
          <Card.Group>
            {priorityList.map((item, index) => (
              <Card fluid key={index}>
                <Card.Content>
                  <Label
                    color={this.getLabelColor(item.priority)}
                    content={item.priority}
                    className="priority-card-label"
                  />
                  {item.task}
                </Card.Content>
                <Button icon="delete" onClick={this.onDeleteHandler(index)} />
              </Card>
            ))}
          </Card.Group>
        </Card.Content>
        <Card.Content>
          <Input
            fluid
            icon={
              <>
                <Dropdown
                  placeholder="Priority"
                  compact
                  search
                  selection
                  onChange={this.onChangeSelectionHandler}
                  value={priority}
                  options={priorityOptions}
                />
                <Button icon="add" onClick={this.onAddItemHandler} />
              </>
            }
            value={input}
            onChange={this.onChangeHandler}
            onKeyPress={this.onKeyPressHandler}
          />
        </Card.Content>
      </Card>
    );
  }
}
export default PriorityCard;
