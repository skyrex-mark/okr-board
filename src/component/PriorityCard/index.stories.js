import { withMarkdownNotes } from "@storybook/addon-notes";
import { storiesOf } from "@storybook/react";
import React from "react";
import PriorityCard from "./index";
import PriorityCardDoc from "./index.md";

storiesOf("Component", module).add(
  "PriorityCard",
  withMarkdownNotes(PriorityCardDoc)(() => <PriorityCard />)
);
