import React from "react";
import { Grid } from "semantic-ui-react";
import ForecastCard from "../ForecastCard";
import HealthCard from "../HealthCard";
import OKRCard from "../OKRCard";
import PriorityCard from "../PriorityCard";

class Board extends React.Component {
  render() {
    return (
      <>
        <Grid padded>
          <Grid.Row>
            <Grid.Column width="4">
              <ForecastCard />
            </Grid.Column>
            <Grid.Column width="4">
              <OKRCard />
            </Grid.Column>
            <Grid.Column width="4">
              <PriorityCard />
            </Grid.Column>
            <Grid.Column width="4">
              <HealthCard />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </>
    );
  }
}
export default Board;
