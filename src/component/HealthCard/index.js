import React from "react";
import { Button, Card, Input, Label } from "semantic-ui-react";
import "./styles.scss";

const statusList = { good: "green", fair: "yellow", bad: "red" };
const getColor = status => statusList[status];
class HealthCard extends React.Component {
  state = {
    healthMetrics: [
      {
        metric: "Team Health",
        notes: ["Team struggling with direction change"],
        status: "fair"
      },
      {
        metric: "Distributor satisfaction Health",
        notes: ["Customers are having trouble to place order online"],
        status: "bad"
      }
    ],
    input: ""
  };
  onChangeHandler = event => {
    this.setState({
      input: event.target.value
    });
  };
  onAddItemHandler = () => {
    this.setState(prevState => ({
      forecastList: [...prevState.forecastList, this.state.input],
      input: ""
    }));
  };
  onDeleteHandler = () => index => {
    // let forecastList = [...this.state.forecastList];
    // forecastList.splice(index, 1);
    // this.setState({
    //   forecastList: forecastList
    // });
  };
  onKeyPressHandler = event => {
    if (event.key === "Enter") {
      this.setState(prevState => ({
        forecastList: [...prevState.forecastList, this.state.input],
        input: ""
      }));
    }
  };

  componentDidMount() {
    //TODO: Add firebase conneciton
  }
  render() {
    const { healthMetrics, input } = this.state;
    return (
      <Card>
        <Card.Content header="Health Metrics" />

        <Card.Content>
          <Card.Content>
            <Card.Group>
              {healthMetrics.map(({ metric, notes, status }, index) => (
                <Card fluid key={metric}>
                  <Card.Content>
                    <Label
                      className="health-card-label"
                      color={getColor(status)}
                    >
                      {status}
                    </Label>
                    {metric}
                  </Card.Content>

                  <Card.Content>
                    {notes.map((item, index) => (
                      <p key={index}>{item}</p>
                    ))}
                  </Card.Content>
                  <Button icon="delete" onClick={this.onDeleteHandler(index)} />
                </Card>
              ))}
            </Card.Group>
          </Card.Content>
        </Card.Content>
        <Card.Content>
          <Input
            fluid
            icon={<Button icon="add" onClick={this.onAddItemHandler} />}
            value={input}
            onChange={this.onChangeHandler}
            onKeyPress={this.onKeyPressHandler}
          />
        </Card.Content>
      </Card>
    );
  }
}
export default HealthCard;
