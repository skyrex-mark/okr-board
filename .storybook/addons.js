import { withNotes } from "@storybook/addon-notes";
import "@storybook/addon-notes/register";
import { addDecorator } from "@storybook/react";

addDecorator(withNotes);
